use serde::{Serialize, Deserialize};
use reqwest::blocking::get; 
fn main() {
    let resp = get("https://api.coingecko.com/api/v3/coins/bitcoin/market_chart?vs_currency=usd&days=0").unwrap().json::<BitcoinResponse>().unwrap();
    println!("{:.0}", resp.prices.get(0).unwrap().get(1).unwrap().round());
}
#[derive(Serialize, Deserialize, Debug)]
struct BitcoinResponse{
    prices: Vec<Vec<f32>>,
    market_caps: Vec<Vec<f32>>,
    total_volumes: Vec<Vec<f32>>,
}